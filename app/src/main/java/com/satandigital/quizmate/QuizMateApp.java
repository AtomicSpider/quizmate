package com.satandigital.quizmate;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.akiniyalocts.imgur_api.ImgurClient;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.parse.Parse;
import com.parse.ParseACL;
import com.satandigital.quizmate.common.Utils;

import java.io.File;

/**
 * Created by Sanat Dutta on 10/9/2015.
 * http://www.satandigital.com/
 */
public class QuizMateApp extends Application {

    private String TAG = "QuizMateApp";

    //Data
    public static SharedPreferences mSharedPreferences;
    public static final String selectedCollegeObjectId = "SELECTED_COLLEGE_OBJECT_ID";
    public static final String selectedCollegeName = "SELECTED_COLLEGE_NAME";
    public static final String selectedDisplayName = "SELECTED_DISPLAY_NAME";

    public static final String showCollegeChooseDialog = "SHOW_COLLEGE_CHOOSE_DIALOG";

    public static final int INVALID_LOGIN_CREDENTIALS = 101;
    public static final int USERNAME_TAKEN = 202;
    public static final int EMAIL_TAKEN = 203;
    public static final int RESET_PASSWORD_EMAIL_NOT_FOUND = 205;

    public static String currentInstituteName = null;
    public static String currentInstituteObjectId = null;
    public static String currentDisplayName = null;

    public static File EXTERNAL_CACHE_DIR;

    @Override
    public void onCreate() {
        super.onCreate();

        mSharedPreferences = getSharedPreferences("QUIZ_MATE_PREF", Context.MODE_PRIVATE);
        EXTERNAL_CACHE_DIR = getExternalCacheDir();

        //Enable Parse crash reports
        //ParseCrashReporting.enable(this);

        //Enable Local Datastore
        //Parse.enableLocalDatastore(this);

        //initialize Parse
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getResources().getString(R.string.parse_server_app_id))
                .server(getResources().getString(R.string.parse_server_uri))
                .build());

        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

        initImageLoader(getApplicationContext());

        // Init Imgur
        ImgurClient.initialize(getResources().getString(R.string.imgur_client_id));

        currentInstituteObjectId = Utils.readSharedPreferences(selectedCollegeObjectId);
        currentInstituteName = Utils.readSharedPreferences(selectedCollegeName);
        currentDisplayName = Utils.readSharedPreferences(selectedDisplayName);
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }
}
