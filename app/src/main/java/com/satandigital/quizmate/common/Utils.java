package com.satandigital.quizmate.common;

import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;

import com.satandigital.quizmate.QuizMateApp;

import java.io.File;

/**
 * Created by Sanat Dutta on 10/9/2015.
 * http://www.satandigital.com/
 */
public class Utils {

    private String TAG = "Utils";

    public static void saveToSharedPreferences(String key, String sValue) {
        SharedPreferences.Editor mEditor = QuizMateApp.mSharedPreferences.edit();
        mEditor.putString(key, sValue);
        mEditor.commit();
    }

    public static void saveToSharedPreferences(String key, boolean bValue) {
        SharedPreferences.Editor mEditor = QuizMateApp.mSharedPreferences.edit();
        mEditor.putBoolean(key, bValue);
        mEditor.commit();
    }

    public static boolean readSharedPreferences(String key, boolean valueDefault) {
        return QuizMateApp.mSharedPreferences.getBoolean(key, valueDefault);
    }

    public static String readSharedPreferences(String key) {
        return QuizMateApp.mSharedPreferences.getString(key, null);
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting());
    }

    public static int dpToPx(int dp, Context mContext) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(int px, Context mContext) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static void downloadFile(Context mContext, String uRl, String fileName) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/QuizMate Downloads");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle(fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDescription("Downloading Image.")
                .setDestinationInExternalPublicDir("/QuizMate Downloads", fileName + ".jpg");

        mgr.enqueue(request);

    }
}
