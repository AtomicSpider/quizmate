package com.satandigital.quizmate.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.satandigital.quizmate.QuizMateApp;
import com.satandigital.quizmate.R;
import com.satandigital.quizmate.activities.ViewResourceActivity;
import com.satandigital.quizmate.common.SquareFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanat Dutta on 10/9/2015.
 * http://www.satandigital.com/
 */
public class SearchFragment extends Fragment {

    private String TAG = "SearchFragment";

    //Interfaces
    private resourceAdapter mAdapterGrid;

    //Views
    private EditText screenSearchEditText;
    private ImageView screenSearchButton, resetSearch;
    private MaterialDialog progressDialog;
    private CircularProgressView fetchingDataProgressBarBelow;
    private TextView searchWordTextView;
    private GridView screenSearchDisplayGrid;
    private LinearLayout screenSearchLinearLayout, screenSearchDisplayLayout;

    //Data
    private ArrayList<ParseObject> resourceObjects = new ArrayList<>();
    private int skipCount = 0;
    private String currentCourseId = "";
    private boolean fetchMore = true, dataFetchOngoing = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        Log.i(TAG, "SearchFragment: onCreateView");

        findViews(view);
        setCustomFonts();
        setOnClickListeners();
        mAdapterGrid = new resourceAdapter();
        screenSearchDisplayGrid.setAdapter(mAdapterGrid);

        return view;
    }

    private void findViews(View view) {
        screenSearchEditText = (EditText) view.findViewById(R.id.screenSearchEditText);
        screenSearchButton = (ImageView) view.findViewById(R.id.screenSearchButton);
        fetchingDataProgressBarBelow = (CircularProgressView) view.findViewById(R.id.fetchingDataProgressBarBelow);
        searchWordTextView = (TextView) view.findViewById(R.id.searchWordTextView);
        screenSearchDisplayGrid = (GridView) view.findViewById(R.id.screenSearchDisplayGrid);
        screenSearchLinearLayout = (LinearLayout) view.findViewById(R.id.screenSearchLinearLayout);
        screenSearchDisplayLayout = (LinearLayout) view.findViewById(R.id.screenSearchDisplayLayout);
        resetSearch = (ImageView) view.findViewById(R.id.resetSearch);

        screenSearchEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    private void setCustomFonts() {
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "font/Roboto-Thin.ttf");
        screenSearchEditText.setTypeface(face);
    }

    private void setOnClickListeners() {
        screenSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (screenSearchEditText.getText().toString().length() > 0) {
                    skipCount = 0;
                    currentCourseId = screenSearchEditText.getText().toString();
                    resourceObjects = new ArrayList<ParseObject>();
                    mAdapterGrid.notifyDataSetChanged();
                    fetchResources(currentCourseId, false);
                } else
                    Toast.makeText(getActivity(), "Please input Course ID", Toast.LENGTH_SHORT).show();
            }
        });
        resetSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenSearchLinearLayout.setVisibility(View.VISIBLE);
                screenSearchDisplayLayout.setVisibility(View.GONE);
            }
        });
    }

    private void fetchResources(final String CourseID, boolean isSkipped) {
        dataFetchOngoing = true;
        searchWordTextView.setText("RESULTS FOR " + CourseID);

        if (QuizMateApp.currentInstituteName != null) {
            ParseQuery<ParseObject> resourceQuery = ParseQuery.getQuery("Resources");
            resourceQuery.whereEqualTo("userInstituteObjectId", QuizMateApp.currentInstituteObjectId);
            resourceQuery.whereEqualTo("resourceCourseId", CourseID);
            resourceQuery.setLimit(8);
            if (isSkipped) resourceQuery.setSkip(skipCount);
            resourceQuery.addDescendingOrder("createdAt");
            if (skipCount == 0) showProgressDialog("Searching...");
            else fetchingDataProgressBarBelow.setVisibility(View.VISIBLE);

            resourceQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    dataFetchOngoing = false;
                    if (skipCount == 0) progressDialog.dismiss();
                    else fetchingDataProgressBarBelow.setVisibility(View.GONE);
                    if (e == null) {
                        if (list.size() > 0) {
                            fetchMore = true;
                            if (skipCount == 0) {
                                Log.i(TAG, "First fetch. Found " + list.size() + "resources");
                                screenSearchLinearLayout.setVisibility(View.GONE);
                                screenSearchDisplayLayout.setVisibility(View.VISIBLE);
                                resourceObjects.addAll(list);
                            } else {
                                Log.i(TAG, list.size() + " resources added");
                                resourceObjects.addAll(list);
                            }
                            mAdapterGrid.notifyDataSetChanged();
                        } else {
                            if (skipCount == 0) {
                                screenSearchLinearLayout.setVisibility(View.VISIBLE);
                                screenSearchDisplayLayout.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "No resources found with the Course ID: " + CourseID + "& College: " + QuizMateApp.currentInstituteName, Toast.LENGTH_SHORT).show();
                            } else {
                                Log.e(TAG, "Fetch Error: list size 0");
                                fetchMore = false;
                                Toast.makeText(getActivity(), "End of search result", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Log.e(TAG, "Error fetching resource list: " + e.getCode() + "" + e.getMessage());
                        if (skipCount == 0)
                            Toast.makeText(getActivity(), "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                        else {
                            fetchMore = false;
                            Log.e(TAG, "Fetch Error: " + e.getMessage());
                            Toast.makeText(getActivity(), "End of search result", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        } else {
            dataFetchOngoing = false;
            new MaterialDialog.Builder(getActivity())
                    .title("Set College")
                    .titleColorRes(R.color.colorPrimaryDark)
                    .content("To search resources you must choose your college first. Set your college from the navigation menu.")
                    .cancelable(true)
                    .positiveText("Choose College")
                    .negativeText("Cancel")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            Intent mShowCollegeDialogIntent = new Intent();
                            mShowCollegeDialogIntent.setAction(QuizMateApp.showCollegeChooseDialog);
                            getActivity().sendBroadcast(mShowCollegeDialogIntent);
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                        }
                    })
                    .show();
        }
    }

    private class resourceAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;

        private resourceAdapter() {
            mLayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return resourceObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return resourceObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View mView = mLayoutInflater.inflate(R.layout.item_resource_grid, parent, false);
            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.mCourseId = (TextView) mView.findViewById(R.id.resource_course_id);
            mViewHolder.mCourseTag = (TextView) mView.findViewById(R.id.resource_course_tag);
            mViewHolder.mCourseYear = (TextView) mView.findViewById(R.id.resource_year);
            mViewHolder.mPositionResource = (TextView) mView.findViewById(R.id.positionResource);
            mViewHolder.mResourceTap = (SquareFrameLayout) mView.findViewById(R.id.resource_tap);

            mViewHolder.mCourseId.setText(resourceObjects.get(position).getString("resourceCourseId"));
            mViewHolder.mCourseTag.setText(resourceObjects.get(position).getString("resourceTag"));
            mViewHolder.mCourseYear.setText(resourceObjects.get(position).getString("resourceYear"));
            int resourceNum = position + 1;
            mViewHolder.mPositionResource.setText("" + resourceNum);

            if (position == resourceObjects.size() - 1 && fetchMore && !dataFetchOngoing) {
                Log.i(TAG, "Fetching More");
                skipCount = resourceObjects.size();
                fetchResources(currentCourseId, true);
            }

            mViewHolder.mResourceTap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mIntent = new Intent(getActivity(), ViewResourceActivity.class);
                    mIntent.putExtra("resourceInstituteName", resourceObjects.get(position).getString("userInstituteName"));
                    mIntent.putExtra("resourceCourseId", resourceObjects.get(position).getString("resourceCourseId"));
                    mIntent.putExtra("resourceTag", resourceObjects.get(position).getString("resourceTag"));
                    mIntent.putExtra("resourceYear", resourceObjects.get(position).getString("resourceYear"));

                    JSONArray mJsonArray = resourceObjects.get(position).getJSONArray("resourceImages");
                    ArrayList<String> mImageStringArray = new ArrayList<String>();

                    for (int i = 0; i < mJsonArray.length(); i++) {
                        try {
                            mImageStringArray.add(mJsonArray.getString(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    mIntent.putStringArrayListExtra("resourceImages", mImageStringArray);
                    startActivity(mIntent);
                }
            });

            return mView;
        }

        private class ViewHolder {
            private TextView mCourseId, mCourseTag, mCourseYear, mPositionResource;
            private SquareFrameLayout mResourceTap;
        }
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(getActivity())
                .content(message)
                .contentColorRes(R.color.colorPrimaryDark)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }
}
