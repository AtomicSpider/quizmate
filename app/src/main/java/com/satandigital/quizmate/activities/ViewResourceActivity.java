package com.satandigital.quizmate.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.satandigital.quizmate.R;
import com.satandigital.quizmate.common.SquareImageView;

import java.util.ArrayList;

/**
 * Created by Sanat Dutta on 2/20/2016.
 */
public class ViewResourceActivity extends AppCompatActivity {

    private String TAG = "ViewResourceActivity";

    //Interfaces
    private imagesAdapter mAdapter;

    //Views
    private GridView mGridView;

    //Data
    private String resourceInstituteName, resourceCourseId, resourceTag, resourceYear;
    private ArrayList<String> resourceImageStringArray;
    private DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        resourceImageStringArray = new ArrayList<>();

        Log.i(TAG, "ViewResourceActivity: onCreate()");
        setContentView(R.layout.activity_view_resource);

        buildImageLoaderOptions();

        getIntentData();
        findViews();
        setUpActionBar();
        mAdapter = new imagesAdapter();
        mGridView.setAdapter(mAdapter);
    }

    private void buildImageLoaderOptions() {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder_image)
                .showImageForEmptyUri(R.drawable.placeholder_image)
                .showImageOnFail(R.drawable.placeholder_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    private void setUpActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
    }

    private void getIntentData() {
        resourceInstituteName = getIntent().getStringExtra("resourceInstituteName");
        resourceCourseId = getIntent().getStringExtra("resourceCourseId");
        resourceTag = getIntent().getStringExtra("resourceTag");
        resourceYear = getIntent().getStringExtra("resourceYear");
        resourceImageStringArray = getIntent().getStringArrayListExtra("resourceImages");
        if (resourceImageStringArray.size() == 0) {
            Toast.makeText(ViewResourceActivity.this, "Could not display images.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void findViews() {
        mGridView = (GridView) findViewById(R.id.photosGridView);
    }

    private class imagesAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;

        private imagesAdapter() {
            mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return resourceImageStringArray.size();
        }

        @Override
        public Object getItem(int position) {
            return resourceImageStringArray.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View mView = mLayoutInflater.inflate(R.layout.item_photo_grid, parent, false);
            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.mImage = (SquareImageView) mView.findViewById(R.id.picture);

            ImageLoader.getInstance().displayImage(resourceImageStringArray.get(position), mViewHolder.mImage, options);

            mViewHolder.mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enlargePhoto(position);
                }
            });

            return mView;
        }

        private class ViewHolder {
            private SquareImageView mImage;
        }
    }

    private void enlargePhoto(int position) {
        Intent mIntent = new Intent(ViewResourceActivity.this, PhotoEnlargeActivity.class);
        mIntent.putStringArrayListExtra("imageUris", resourceImageStringArray);
        mIntent.putExtra("title", resourceInstituteName + "_" + resourceCourseId + "_" + resourceTag + "_" + resourceYear + "_");
        mIntent.putExtra("position", position);
        startActivity(mIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
