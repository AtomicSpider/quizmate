package com.satandigital.quizmate.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.satandigital.quizmate.QuizMateApp;
import com.satandigital.quizmate.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sanat Dutta on 10/28/2015.
 * http://www.satandigital.com/
 */
public class EmailLoginActivity extends AppCompatActivity {

    private String TAG = "EmailLoginActivity";

    //Interface
    MaterialDialog progressDialog, mVerifyDialog;

    //Views
    private EditText usernameEditText, passwordEditText;
    private TextView loginButton, forgotPassword;
    private View positiveAction;

    //Data
    private String emailEditText;
    private boolean isUnverifiedUserPresent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "EmailLoginActivity: onCreate()");
        setContentView(R.layout.activity_email_login);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        findViews();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String errorString = logInError();

                if (errorString == null) {
                    loginParse(usernameEditText.getText().toString(), passwordEditText.getText().toString());
                } else
                    Toast.makeText(EmailLoginActivity.this, "" + errorString, Toast.LENGTH_SHORT).show();
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showResetDialog();
            }
        });
    }

    private void findViews() {
        usernameEditText = (EditText) findViewById(R.id.email_login_userNameEditText);
        passwordEditText = (EditText) findViewById(R.id.email_login_passWordEditText);
        loginButton = (TextView) findViewById(R.id.email_login_loginButton);
        forgotPassword = (TextView) findViewById(R.id.email_login_forgotPassword);
    }

    private String logInError() {

        if (usernameEditText.getText().toString().contains(" ")) {
            return "Username cannot contain blank spaces";
        }
        if (usernameEditText.getText().toString().equals("")) {
            return "Username field is empty";
        }
        if (passwordEditText.getText().toString().contains(" ")) {
            return "Password cannot contain blank spaces";
        }
        if (passwordEditText.getText().toString().equals("")) {
            return "Password field is empty";
        } else return null;
    }

    private void loginParse(String userName, String passWord) {
        showProgressDialog("Logging In...");
        ParseUser.logInInBackground(userName, passWord, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    if (parseUser.getBoolean("emailVerified")) {
                        isUnverifiedUserPresent = false;
                        goToMainActivity();
                    } else {
                        isUnverifiedUserPresent = true;
                        showVerifyEmailDialog();
                    }
                } else {
                    if (e.getCode() == QuizMateApp.INVALID_LOGIN_CREDENTIALS)
                        Toast.makeText(EmailLoginActivity.this, "Email or Password is incorrect", Toast.LENGTH_SHORT).show();
                    else {
                        Log.e(TAG, "Login error:" + e.getCode() + " " + e.getMessage());
                        Toast.makeText(EmailLoginActivity.this, "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void showVerifyEmailDialog() {
        mVerifyDialog = new MaterialDialog.Builder(this)
                .title("Account Not Verified")
                .titleColorRes(R.color.colorPrimaryDark)
                .content("Your email: " + ParseUser.getCurrentUser().getEmail() + " is not verified. Please check your email for the verification email. If you didn't receive a verification email, please check your spam folder.")
                .positiveText("Okay")
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Log.i(TAG, "Okay");
                        ParseUser.logOutInBackground();
                    }
                }).build();

        mVerifyDialog.show();
    }

    private void showResetDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Password Reset")
                .titleColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_reset_password, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        showProgressDialog("Sending Email...");
                        ParseUser.requestPasswordResetInBackground(emailEditText,
                                new RequestPasswordResetCallback() {
                                    public void done(ParseException e) {
                                        progressDialog.dismiss();
                                        if (e == null) {
                                            showCheckEmailDialog();
                                        } else {
                                            if (e.getCode() == QuizMateApp.RESET_PASSWORD_EMAIL_NOT_FOUND)
                                                Toast.makeText(EmailLoginActivity.this, "Entered email is not registered", Toast.LENGTH_LONG).show();
                                            else {
                                                Toast.makeText(EmailLoginActivity.this, "Oops, Something went wrong", Toast.LENGTH_LONG).show();
                                                Log.i(TAG, "Reset password failed:" + e.getCode() + " " + e.getMessage());
                                            }
                                        }
                                    }
                                });
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        EditText emailInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_reset_email);
        emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isEmailValid(s.toString().trim())) {
                    emailEditText = s.toString().trim();
                    positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    private void showCheckEmailDialog() {
        new MaterialDialog.Builder(this)
                .content("Please check your email for password reset instructions")
                .positiveText("Okay")
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Log.i(TAG, "Okay");
                    }
                }).build().show();
    }

    public boolean isEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.colorPrimaryDark)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private void goToMainActivity() {
        Intent loginSuccessfulIntent = new Intent();
        loginSuccessfulIntent.putExtra("LOGIN_STATUS", "SUCCESS");
        setResult(Activity.RESULT_OK, loginSuccessfulIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        if (mVerifyDialog != null) {
            if (mVerifyDialog.isShowing()) mVerifyDialog.dismiss();
        }

        if (isUnverifiedUserPresent) {
            Log.i(TAG, "Unverified user logged out");
            ParseUser.logOut();
        }
        super.onPause();
    }
}
