package com.satandigital.quizmate.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.satandigital.quizmate.R;
import com.satandigital.quizmate.common.DepthPageTransformer;
import com.satandigital.quizmate.common.ExtendedViewPager;
import com.satandigital.quizmate.common.TouchImageView;
import com.satandigital.quizmate.common.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Sanat Dutta on 8/30/2015.
 */
public class PhotoEnlargeActivity extends AppCompatActivity {

    private String TAG = "PhotoEnlargeActivity";

    //Interface
    private ExtendedViewPager mViewPager;
    private CustomPageAdapter mAdapter;
    private AsyncTask mShareImage;

    //View
    private MaterialDialog progressDialog;

    //Data
    private DisplayImageOptions options;
    private ArrayList<String> imageUris;
    private String mTitle;
    private int currentPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "PhotoEnlargeActivity: onCreate()");

        setContentView(R.layout.activity_photo_enlarge);

        getIntentData();

        setUpActionBar();
        setUpViewPager();
        setUpViewPagerOnClickListener();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.photo_enlarge_placeholder_image)
                .showImageForEmptyUri(R.drawable.photo_enlarge_placeholder_image)
                .showImageOnFail(R.drawable.photo_enlarge_placeholder_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    private void setUpViewPagerOnClickListener() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                getSupportActionBar().setTitle("Image " + String.valueOf(position + 1) + "/" + String.valueOf(imageUris.size()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setUpViewPager() {
        mViewPager = (ExtendedViewPager) findViewById(R.id.pager);
        mViewPager.setPageTransformer(true, new DepthPageTransformer());
        mAdapter = new CustomPageAdapter(this);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(currentPosition);
    }

    private void getIntentData() {
        imageUris = new ArrayList<>();
        imageUris = getIntent().getStringArrayListExtra("imageUris");
        currentPosition = getIntent().getExtras().getInt("position");
        mTitle = getIntent().getStringExtra("title");
    }

    private void setUpActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("Image " + String.valueOf(currentPosition + 1) + "/" + String.valueOf(imageUris.size()));
    }

    private class CustomPageAdapter extends PagerAdapter {

        private Context mContext;
        private LayoutInflater mLayoutInflater;

        public CustomPageAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getItemPosition(Object object) {
            //return super.getItemPosition(object);
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return imageUris.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.touch_image_view, container, false);
            TouchImageView mTouchImageView = (TouchImageView) itemView.findViewById(R.id.photoImage);
            String mString = imageUris.get(position);

            ImageLoader.getInstance().displayImage(mString, mTouchImageView, options);

            mTouchImageView.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    return false;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    return false;
                }
            });

            container.addView(itemView, 0);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
            View view = (View) object;
            view = null;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.photoenlarge_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.nav_pen_download:
                Utils.downloadFile(PhotoEnlargeActivity.this, imageUris.get(currentPosition), mTitle + currentPosition);
                return true;
            case R.id.nav_pen_share:
                Log.i(TAG, "Share Image");
                mShareImage = new shareAsync(PhotoEnlargeActivity.this).execute(imageUris.get(currentPosition));
                return true;
            case R.id.nav_pen_report:
                //ToDo
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(PhotoEnlargeActivity.this)
                .content(message)
                .contentColorRes(R.color.colorPrimaryDark)
                .cancelable(true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        mShareImage.cancel(true);
                    }
                })
                .progress(true, 0)
                .show();
    }

    public class shareAsync extends AsyncTask<String, String, String> {
        private Context mContext;
        URL myFileUrl;

        Bitmap bmImg = null;
        Intent share;
        File file;
        boolean isCancelled = false;

        public shareAsync(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            isCancelled = true;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
            showProgressDialog("Downloading High Resolution Image for Sharing...");

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            HttpURLConnection conn = null;

            try {
                if (!isCancelled()) {
                    myFileUrl = new URL(args[0]);
                    conn = (HttpURLConnection) myFileUrl.openConnection();
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    bmImg = BitmapFactory.decodeStream(is);
                } else {
                    if (conn != null) conn.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {

                String path = myFileUrl.getPath();
                String idStr = path.substring(path.lastIndexOf('/') + 1);
                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/Google Image Wallpaper/");
                dir.mkdirs();
                String fileName = idStr;
                file = new File(dir, fileName);
                FileOutputStream fos = new FileOutputStream(file);
                bmImg.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String args) {
            // TODO Auto-generated method stub
            progressDialog.dismiss();
            share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse(file.getAbsolutePath().toString()));
            mContext.startActivity(Intent.createChooser(share, "Share Image"));
        }

    }
}