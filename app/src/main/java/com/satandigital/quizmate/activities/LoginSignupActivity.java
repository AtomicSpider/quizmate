package com.satandigital.quizmate.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.satandigital.quizmate.R;

/**
 * Created by Sanat Dutta on 10/28/2015.
 * http://www.satandigital.com/
 */
public class LoginSignupActivity extends AppCompatActivity {

    private String TAG = "LoginSignupActivity";

    //Views
    private TextView loginTextView, signUpTextView;

    //Data
    private int LOGIN_REQUEST_CODE = 71;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "LoginSignupActivity: onCreate()");
        setContentView(R.layout.activity_login_signup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        findViews();
        setOnClickListeners();
    }

    private void findViews() {
        loginTextView = (TextView) findViewById(R.id.loginLoginButton);
        signUpTextView = (TextView) findViewById(R.id.loginSignUpButton);
    }

    private void setOnClickListeners() {
        loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToEmailLoginActivity = new Intent(LoginSignupActivity.this, EmailLoginActivity.class);
                startActivityForResult(goToEmailLoginActivity, LOGIN_REQUEST_CODE);
            }
        });
        signUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToEmailSignUpActivity = new Intent(LoginSignupActivity.this, EmailSignUpActivity.class);
                startActivity(goToEmailSignUpActivity);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == LOGIN_REQUEST_CODE && data.getStringExtra("LOGIN_STATUS").equals("SUCCESS")) {
            goToMainActivity();
        }
    }

    private void goToMainActivity() {
        Intent loginSuccessfulIntent = new Intent();
        loginSuccessfulIntent.putExtra("LOGIN_STATUS", "SUCCESS");
        setResult(Activity.RESULT_OK, loginSuccessfulIntent);
        finish();
    }
}
