package com.satandigital.quizmate.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.parse.FindCallback;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.satandigital.quizmate.QuizMateApp;
import com.satandigital.quizmate.R;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sanat Dutta on 10/28/2015.
 * http://www.satandigital.com/
 */
public class EmailSignUpActivity extends AppCompatActivity {

    private String TAG = "EmailSignUpActivity";

    //Interface
    MaterialDialog progressDialog;

    //Views
    private Spinner collegeSpinner;
    private EditText usernameEditText, emailEditText, passwordEditText, passwordAgainEditText;
    private TextView signUpButton;
    private FrameLayout spinnerBG;

    //Data
    private int MAX_USERNAME_LENGTH = 20;
    private ArrayList<ParseObject> collegeList;
    private int collegeSpinnerPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "EmailSignUpActivity: onCreate()");
        setContentView(R.layout.activity_email_signup);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        findViews();

        fetchCollegeList();
    }

    private void findViews() {
        usernameEditText = (EditText) findViewById(R.id.email_signup_userNameEditText);
        spinnerBG = (FrameLayout) findViewById(R.id.spinnerBG);
        collegeSpinner = (Spinner) findViewById(R.id.email_signup_collegeSpinner);
        emailEditText = (EditText) findViewById(R.id.email_signup_emailEditText);
        passwordEditText = (EditText) findViewById(R.id.email_signup_passWordEditText);
        passwordAgainEditText = (EditText) findViewById(R.id.email_signup_passWordAgainEditText);
        signUpButton = (TextView) findViewById(R.id.email_signup_signUpButton);
    }

    private void fetchCollegeList() {
        ParseQuery<ParseObject> collegeListQuery = ParseQuery.getQuery("Colleges");
        collegeListQuery.addAscendingOrder("instituteName");
        showProgressDialog("Please wait...");
        collegeListQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    collegeList = new ArrayList<>();
                    collegeList.addAll(list);

                    setUpSpinner();
                    spinnerBG.setVisibility(View.VISIBLE);
                    setUpSpinnerListener();
                    setSignUpButtonOnClickListener();
                } else {
                    Log.e(TAG, "Error fetching college list: " + e.getCode() + "" + e.getMessage());
                    Toast.makeText(EmailSignUpActivity.this, "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    private void setUpSpinner() {

        ArrayList<String> collegeListString = new ArrayList<>();

        for (int i = 0; i < collegeList.size(); i++) {
            collegeListString.add(collegeList.get(i).getString("instituteName"));
        }

        ArrayAdapter<String> mAdapter = new ArrayAdapter<>(this, R.layout.signup_spinner_item, collegeListString);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        collegeSpinner.setAdapter(mAdapter);
    }

    private void setUpSpinnerListener() {
        collegeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                collegeSpinnerPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSignUpButtonOnClickListener() {
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String errorString = signUpError();

                if (errorString == null) {
                    signUpParse(usernameEditText.getText().toString(), collegeSpinnerPosition, emailEditText.getText().toString(), passwordEditText.getText().toString());
                } else
                    Toast.makeText(EmailSignUpActivity.this, "" + errorString, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String signUpError() {

        if (usernameEditText.getText().toString().contains(" ")) {
            return "Username cannot contain blank spaces";
        }
        if (usernameEditText.getText().toString().equals("")) {
            return "Username field is empty";
        }
        if (usernameEditText.getText().toString().length() > MAX_USERNAME_LENGTH) {
            return "Username must be " + MAX_USERNAME_LENGTH + " characters or less";
        }
        if (emailEditText.getText().toString().contains(" ")) {
            return "Email cannot contain blank spaces";
        }
        if (emailEditText.getText().toString().equals("")) {
            return "Email field is empty";
        }
        if (!isEmailValid(emailEditText.getText().toString())) {
            return "Email is not valid, please make sure you enter the email address provided by your institute";
        }
        if (passwordEditText.getText().toString().contains(" ")) {
            return "Password cannot contain blank spaces";
        }
        if (passwordEditText.getText().toString().equals("")) {
            return "Password field is empty";
        }
        if (passwordEditText.getText().toString().length() < 4) {
            return "Password must be 4 characters or more";
        }
        if (!passwordAgainEditText.getText().toString().equals(passwordEditText.getText().toString())) {
            return "Passwords do not match";
        } else return null;
    }

    private void signUpParse(final String userName, final int position, final String email, String passWord) {

        showProgressDialog("Signing Up...");

        ParseUser newUser = new ParseUser();
        newUser.setUsername(userName.toLowerCase());
        newUser.setPassword(passWord);
        newUser.setEmail(email);
        newUser.put("displayName", userName);
        newUser.put("instituteName", collegeList.get(position).getString("instituteName"));
        newUser.put("instituteObjectId", collegeList.get(position).getObjectId());

        newUser.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    setUpUserStatusClass(ParseUser.getCurrentUser().getObjectId(), userName, collegeList.get(position).getString("instituteName"), collegeList.get(position).getObjectId(), email);
                } else {
                    if (e.getCode() == QuizMateApp.USERNAME_TAKEN)
                        Toast.makeText(EmailSignUpActivity.this, "Username Already Taken", Toast.LENGTH_SHORT).show();
                    else if (e.getCode() == QuizMateApp.EMAIL_TAKEN)
                        Toast.makeText(EmailSignUpActivity.this, "Email Already Taken", Toast.LENGTH_SHORT).show();
                    else {
                        Log.e(TAG, "Signup Error: " + e.getCode() + " " + e.getMessage());
                        Toast.makeText(EmailSignUpActivity.this, "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void setUpUserStatusClass(String userId, String userName, String instituteName, String instituteObjectId, String email) {
        showProgressDialog("Setting up account...");

        ParseObject userStatusObject = new ParseObject("UserStatus");
        userStatusObject.put("userObjectId", userId);
        userStatusObject.put("userName", userName.toLowerCase());
        userStatusObject.put("displayName", userName);
        userStatusObject.put("email", email);
        userStatusObject.put("instituteName", instituteName);
        userStatusObject.put("instituteObjectId", instituteObjectId);
        userStatusObject.put("banned", false);

        userStatusObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                ParseUser.logOutInBackground(new LogOutCallback() {
                    @Override
                    public void done(ParseException e) {
                        progressDialog.dismiss();
                        if (e == null) {
                            Log.e(TAG, "Logged Out");
                        } else
                            Log.e(TAG, "Could not log out. Error: " + e.getCode() + " " + e.getMessage());

                        showInfoDialog();
                    }
                });
            }
        });
    }

    private void showInfoDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Verify Account")
                .titleColorRes(R.color.colorPrimaryDark)
                .content("Please check your email for the verification email and login again")
                .positiveText("Okay")
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Log.i(TAG, "Okay");
                        finish();
                    }
                }).build();

        dialog.show();
    }

    public boolean isEmailValid(String email) {
        String domainRegEx = collegeList.get(collegeSpinnerPosition).getString("emailDomain");

        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + domainRegEx + "$";

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.colorPrimaryDark)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
