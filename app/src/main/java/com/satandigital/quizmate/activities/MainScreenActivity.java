package com.satandigital.quizmate.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.akiniyalocts.imgur_api.ImgurClient;
import com.akiniyalocts.imgur_api.model.Image;
import com.akiniyalocts.imgur_api.model.ImgurResponse;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.satandigital.quizmate.QuizMateApp;
import com.satandigital.quizmate.R;
import com.satandigital.quizmate.common.DocumentHelper;
import com.satandigital.quizmate.common.RateTheApp;
import com.satandigital.quizmate.common.SquareImageView;
import com.satandigital.quizmate.common.Utils;
import com.satandigital.quizmate.common.WrappingGridView;
import com.satandigital.quizmate.fragments.CourseFragment;
import com.satandigital.quizmate.fragments.SearchFragment;
import com.satandigital.quizmate.models.ImageObjects;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static com.satandigital.quizmate.common.RateTheApp.setOptOut;

/**
 * Created by Sanat Dutta on 10/9/2015.
 * http://www.satandigital.com/
 */
public class MainScreenActivity extends AppCompatActivity {

    private String TAG = "MainScreenActivity";

    //Interface
    private Toolbar mToolbar;
    private android.support.v7.app.ActionBar mActionBar;
    private TabLayout mTabLayout;
    private MainScreenPagerAdapter mMainScreenPagerAdapter;
    private ViewPager mViewPager;
    private ActionBarDrawerToggle mDrawerToggle;
    private imageAdapter mAdapterGrid;

    //Views
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private View mHeaderView;
    private MaterialDialog progressDialog;
    private MaterialDialog progressBar;
    private FloatingActionButton actionAddQuizPaper;
    private CircleImageView currentUserImage;
    private TextView userName, userCollege;
    private View positiveAction;

    //Data
    private String feedbackDialogSubject, feedbackDialogMessage;
    private DisplayImageOptions options;
    private int resourceTagPos = 0;
    private String resourceCourseCode = "", resourceTag = "", resourceYear = "";
    public static String resourceTitle = "";
    private String imgurAlbumID = "", imgurType = "", imgurDescription = "";

    private ArrayList<com.satandigital.quizmate.models.ImageObjects> imageObjects;
    private Uri mImageUri;

    private int CHOOSE_IMAGE_FROM_CAMERA = 11;
    private int CHOOSE_IMAGE_FROM_GALLERY = 12;

    public static int currentUploadIndex = 0;
    private ArrayList<String> imageUrlList;

    private String tempDisplayName = null;

    private ArrayList<ParseObject> collegeList;
    private int collegeSpinnerPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "MainScreenActivity: onCreate()");

        setContentView(R.layout.activity_main_screen);
        buildImageLoaderOptions();
        findViews();

        setUpActionBar();
        setUpViewPager();
        setPageChangeListener();
        setUpNavigationDrawer();
        setDrawerOnClickListeners();
        setHeader();
        registerForBroadcasts();

        getImgurTags();

        //toggleAppropriateNavItems();
        setUpOnClickListeners();
        setAppRate();
        saveInstallationToParse();

        mAdapterGrid = new imageAdapter();

    }

    private void getImgurTags() {
        imgurAlbumID = getResources().getString(R.string.imgur_image_album_id);
        imgurType = getResources().getString(R.string.imgur_image_type);
        imgurDescription = getResources().getString(R.string.imgur_image_description);
    }

    private void registerForBroadcasts() {
        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(QuizMateApp.showCollegeChooseDialog);
        registerReceiver(mReceiver, mFilter);
    }

    private void buildImageLoaderOptions() {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder_image)
                .showImageForEmptyUri(R.drawable.placeholder_image)
                .showImageOnFail(R.drawable.placeholder_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    private void findViews() {
        actionAddQuizPaper = (FloatingActionButton) findViewById(R.id.action_add);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mHeaderView = mNavigationView.getHeaderView(0);
        currentUserImage = (CircleImageView) mHeaderView.findViewById(R.id.current_user_image);
        userName = (TextView) mHeaderView.findViewById(R.id.current_username);
        userCollege = (TextView) mHeaderView.findViewById(R.id.current_college);
    }

    private void setUpActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setTitle(getResources().getString(R.string.app_name));
    }

    private void setUpViewPager() {
        mMainScreenPagerAdapter = new MainScreenPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mMainScreenPagerAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.setTabsFromPagerAdapter(mMainScreenPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

    private void setPageChangeListener() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();

                switch (position) {
                    case 0:
                        actionAddQuizPaper.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        actionAddQuizPaper.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setUpNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.d(TAG, "Drawer Opened");
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d(TAG, "Drawer Closed");
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    private void setDrawerOnClickListeners() {

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.nav_choose_name:
                        showDisplayNameChooserDialog();
                        break;
                    case R.id.nav_choose_college:
                        showCollegeChooserDialog();
                        break;
                    case R.id.nav_about:
                        showAboutDialog();
                        break;
                    case R.id.nav_share:
                        shareApp();
                        break;
                    case R.id.nav_rate:
                        Intent mIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.satandigital.quizmate"));
                        startActivity(mIntent);
                        setOptOut(MainScreenActivity.this, true);
                        break;
                    case R.id.nav_feedback:
                        showFeedbackDialog();
                        break;
                    case R.id.nav_contact:
                        contactIntent();
                        break;
                }

                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    private void setHeader() {
        if (QuizMateApp.currentDisplayName != null)
            userName.setText(QuizMateApp.currentDisplayName);
        else userName.setText("Anonymous");
        if (QuizMateApp.currentInstituteName != null)
            userCollege.setText(QuizMateApp.currentInstituteName);
        else userCollege.setText("College");
    }

    public void showCollegeChooserDialog() {
        ParseQuery<ParseObject> collegeListQuery = ParseQuery.getQuery("Colleges");
        collegeListQuery.addAscendingOrder("instituteName");
        showProgressDialog("Please wait...");
        collegeListQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    collegeList = new ArrayList<>();
                    collegeList.addAll(list);
                    displayCollegeChooserDialog();
                } else {
                    Log.e(TAG, "Error fetching college list: " + e.getCode() + "" + e.getMessage());
                    Toast.makeText(MainScreenActivity.this, "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void displayCollegeChooserDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Set College")
                .titleColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_set_college, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        userCollege.setText(collegeList.get(collegeSpinnerPosition).getString("instituteName"));
                        QuizMateApp.currentInstituteName = collegeList.get(collegeSpinnerPosition).getString("instituteName");
                        QuizMateApp.currentInstituteObjectId = collegeList.get(collegeSpinnerPosition).getObjectId();
                        Utils.saveToSharedPreferences(QuizMateApp.selectedCollegeName, collegeList.get(collegeSpinnerPosition).getString("instituteName"));
                        Utils.saveToSharedPreferences(QuizMateApp.selectedCollegeObjectId, collegeList.get(collegeSpinnerPosition).getObjectId());
                        Toast.makeText(MainScreenActivity.this, "College Saved", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        Spinner collegeSpinner = (Spinner) dialog.getCustomView().findViewById(R.id.dialog_collegeSpinner);
        ArrayList<String> collegeListString = new ArrayList<>();

        for (int i = 0; i < collegeList.size(); i++) {
            collegeListString.add(collegeList.get(i).getString("instituteName"));
        }

        ArrayAdapter<String> mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, collegeListString);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        collegeSpinner.setAdapter(mAdapter);

        collegeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                collegeSpinnerPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialog.show();
    }

    private void showDisplayNameChooserDialog() {

        tempDisplayName = null;

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Set Display Name")
                .titleColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_set_displayname, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        if (tempDisplayName.length() > 15)
                            Toast.makeText(MainScreenActivity.this, "Name cannot exceed 15 characters. Use just your first name maybe?", Toast.LENGTH_SHORT).show();
                        else {
                            userName.setText(tempDisplayName);
                            QuizMateApp.currentDisplayName = tempDisplayName;
                            Utils.saveToSharedPreferences(QuizMateApp.selectedDisplayName, tempDisplayName);
                            Toast.makeText(MainScreenActivity.this, "DisplayName Saved", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        EditText displayNameInput = (EditText) dialog.getCustomView().findViewById(R.id.setDisplayNameEditText);
        if (QuizMateApp.currentDisplayName != null)
            displayNameInput.setText(QuizMateApp.currentDisplayName);
        displayNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    tempDisplayName = s.toString().trim();
                    positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    /*private void toggleToLogInNavItems() {
        mNavigationView.getMenu().findItem(R.id.nav_login).setVisible(false);
        mNavigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
        mNavigationView.getMenu().findItem(R.id.nav_choose_college).setVisible(false);
        mNavigationView.getMenu().findItem(R.id.nav_edit_profile).setVisible(true);
    }

    private void toggleToLogOutNavItems() {
        mNavigationView.getMenu().findItem(R.id.nav_login).setVisible(true);
        mNavigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
        mNavigationView.getMenu().findItem(R.id.nav_choose_college).setVisible(true);
        mNavigationView.getMenu().findItem(R.id.nav_edit_profile).setVisible(false);
    }*/

    private void showAboutDialog() {
        new MaterialDialog.Builder(this)
                .title("About")
                .titleColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_about, true)
                .positiveText("Cool")
                .positiveColorRes(R.color.colorPrimaryDark)
                .show();
    }

    private void shareApp() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "QuizMate");
            String sAux = "\nYo !! Check out this App\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.satandigital.quizmate\n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Share App Via"));
        } catch (Exception e) {
            Log.e(TAG, "Error Sharing App: " + e);
            Toast.makeText(MainScreenActivity.this, "Oops, Cannot share the app", Toast.LENGTH_SHORT).show();
        }
    }

    private void showFeedbackDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Feedback | Bug Report")
                .titleColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_feedback, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        sendFeedback();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);

        final EditText subjectInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_feedback_subject);
        subjectInput.requestFocus();
        final EditText messageInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_feedback_message);

        subjectInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    feedbackDialogSubject = s.toString().trim();
                    if (messageInput.getText().toString().trim().length() > 0)
                        positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        messageInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    feedbackDialogMessage = s.toString().trim();
                    if (subjectInput.getText().toString().trim().length() > 0)
                        positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    private void sendFeedback() {
        showProgressDialog("Sending Feedback...");
        String versionName = "N/A";
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        ParseObject feedback = new ParseObject("Feedback");
        feedback.put("appVersion", versionName);
        feedback.put("subject", feedbackDialogSubject);
        feedback.put("message", feedbackDialogMessage);
        if (QuizMateApp.currentDisplayName != null) {
            feedback.put("userName", QuizMateApp.currentDisplayName);
        } else {
            feedback.put("userName", "Anonymous");
        }
        if (QuizMateApp.currentInstituteName != null) {
            feedback.put("instituteName", QuizMateApp.currentInstituteName);
        } else {
            feedback.put("instituteName", "Anonymous");
        }
        feedback.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    Toast.makeText(MainScreenActivity.this, "Feedback Sent", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainScreenActivity.this, "Oops ! Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void contactIntent() {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "iitumstudant@gmail.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Quiz Mate App");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        } catch (Exception e) {
            Toast.makeText(MainScreenActivity.this, "Cannot find any Email app", Toast.LENGTH_SHORT).show();
        }

    }

    /*private void toggleAppropriateNavItems() {
        if (ParseUser.getCurrentUser() != null) {

            QuizMateApp.currentInstituteObjectId = ParseUser.getCurrentUser().getString("instituteObjectId");
            QuizMateApp.currentInstituteName = ParseUser.getCurrentUser().getString("instituteName");

            Utils.saveToSharedPreferences(QuizMateApp.selectedCollegeObjectId, ParseUser.getCurrentUser().getString("instituteObjectId"));
            Utils.saveToSharedPreferences(QuizMateApp.selectedCollegeName, ParseUser.getCurrentUser().getString("instituteName"));

            toggleToLogInNavItems();
            if (ParseUser.getCurrentUser().getParseFile("profileImageFile") != null)
                ImageLoader.getInstance().displayImage(ParseUser.getCurrentUser().getParseFile("profileImageFile").getUrl(), currentUserImage, options);
            else currentUserImage.setImageResource(R.drawable.ic_profile_image_default);
            userName.setText(ParseUser.getCurrentUser().getUsername());
            userCollege.setText(ParseUser.getCurrentUser().getString("instituteName"));
        } else {
            toggleToLogOutNavItems();
            currentUserImage.setImageResource(R.drawable.ic_profile_image_default);
            userName.setText("Anonymous");
            if (QuizMateApp.currentInstituteName != null)
                userCollege.setText(QuizMateApp.currentInstituteName);
            else userCollege.setText("College");
        }
    }*/

    private void setUpOnClickListeners() {
        actionAddQuizPaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (QuizMateApp.currentInstituteName != null)
                    showAddResourceDialog();
                else chooseCollegeInfo();
            }
        });
    }

    private void chooseCollegeInfo() {
        new MaterialDialog.Builder(this)
                .title("Set College")
                .titleColorRes(R.color.colorPrimaryDark)
                .content("To submit resources you must choose your college first. Set your college from the navigation menu.")
                .cancelable(true)
                .positiveText("Choose College")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        showCollegeChooserDialog();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                })
                .show();
    }

    private void showAddResourceDialog() {

        resourceCourseCode = "";
        resourceTagPos = 0;
        resourceTag = "";
        resourceYear = "";
        imageObjects = new ArrayList<>();

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Resource -> " + QuizMateApp.currentInstituteName)
                .titleColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_add_resource, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        uploadResource();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);

        View view = dialog.getCustomView();
        final EditText courseCodeEditText = (EditText) view.findViewById(R.id.dialog_add_resource_course_code);
        final Spinner resourceTagSpinner = (Spinner) view.findViewById(R.id.resource_tag_spinner);
        final EditText resourceTagEditText = (EditText) view.findViewById(R.id.dialog_add_resource_tag);
        final EditText resourceYearEditText = (EditText) view.findViewById(R.id.dialog_add_resource_year);
        WrappingGridView mGridView = (WrappingGridView) view.findViewById(R.id.imagesGridView);

        //Set Layout Properties
        courseCodeEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        resourceTagEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        ArrayAdapter<CharSequence> mAdapter = ArrayAdapter.createFromResource(this,
                R.array.resource_tags, R.layout.add_resource_spinner_item);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        resourceTagSpinner.setAdapter(mAdapter);

        mGridView.setAdapter(mAdapterGrid);

        courseCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                resourceCourseCode = s.toString().trim();
                if (resourceCourseCode.length() > 0 && resourceYear.length() > 0 && imageObjects.size() > 0) {
                    if (resourceTagPos == 4 && resourceTag.length() == 0) {
                        positiveAction.setEnabled(false);
                    } else positiveAction.setEnabled(true);

                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        resourceTagSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                resourceTagPos = position;
                if (position == 4) resourceTagEditText.setVisibility(View.VISIBLE);
                else resourceTagEditText.setVisibility(View.GONE);

                if (resourceCourseCode.length() > 0 && resourceYear.length() > 0 && imageObjects.size() > 0) {
                    if (resourceTagPos == 4 && resourceTag.length() == 0) {
                        positiveAction.setEnabled(false);
                    } else positiveAction.setEnabled(true);

                } else positiveAction.setEnabled(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        resourceTagEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                resourceTag = s.toString().trim();
                if (resourceCourseCode.length() > 0 && resourceYear.length() > 0 && imageObjects.size() > 0) {
                    if (resourceTagPos == 4 && resourceTag.length() == 0) {
                        positiveAction.setEnabled(false);
                    } else positiveAction.setEnabled(true);

                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        resourceYearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                resourceYear = s.toString().trim();
                if (resourceCourseCode.length() > 0 && resourceYear.length() > 0 && imageObjects.size() > 0) {
                    if (resourceTagPos == 4 && resourceTag.length() == 0) {
                        positiveAction.setEnabled(false);
                    } else positiveAction.setEnabled(true);

                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        dialog.show();
        positiveAction.setEnabled(false);
    }

    private void uploadResource() {
        currentUploadIndex = 0;
        if (resourceTagPos != 4)
            resourceTag = getResources().getStringArray(R.array.resource_tags)[resourceTagPos];

        resourceTitle = resourceCourseCode + "_" + resourceTag + "_" + resourceYear + "_";
        imageUrlList = new ArrayList<>();
        uploadNextImageToImgur();
    }

    private void uploadNextImageToImgur() {

        if (currentUploadIndex != imageObjects.size()) {
            if (progressBar != null) {
                if (!progressBar.isShowing())
                    showProgressBar("Uploading Image " + (currentUploadIndex + 1) + "/" + imageObjects.size() + "...");
                else
                    updateProgressBar("Uploading Image " + (currentUploadIndex + 1) + "/" + imageObjects.size() + "...");
            } else
                showProgressBar("Uploading Image " + (currentUploadIndex + 1) + "/" + imageObjects.size() + "...");

            ImgurClient.getInstance()
                    .uploadImage(
                            new TypedFile("image/*", imageObjects.get(currentUploadIndex).getmImageFile()),
                            resourceTitle + "_" + currentUploadIndex,
                            getResources().getString(R.string.imgur_image_description),
                            new Callback<ImgurResponse<Image>>() {
                                @Override
                                public void success(ImgurResponse<Image> imageImgurResponse, Response response) {
                                    Log.i(TAG, "Uploaded image " + currentUploadIndex + " Link: " + imageImgurResponse.data.getLink());
                                    currentUploadIndex += 1;
                                    imageUrlList.add(imageImgurResponse.data.getLink());
                                    uploadNextImageToImgur();
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    if (error == null)
                                        Toast.makeText(MainScreenActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                                    else {
                                        progressBar.dismiss();
                                        Toast.makeText(MainScreenActivity.this, "Oops, something went wrong", Toast.LENGTH_SHORT).show();
                                        Log.e(TAG, "Imgur upload failed. Error: " + error.getMessage());
                                    }
                                }
                            }
                    );

        } else {
            Log.i(TAG, "Uploaded all images");
            progressBar.dismiss();
            saveResourceObject();
        }
    }

    /*private void uploadAllParseFiles() {
        showProgressBar("Uploading Image " + (currentUploadIndex + 1) + "/" + imageObjects.size() + "...");

        final ParseFile mParseFile = new ParseFile(resourceCourseCode + "_" + resourceTag + "_" + resourceYear + "_" + (currentUploadIndex + 1) + ".jpg", imageObjects.get(currentUploadIndex).getmByteArray());
        mParseFile.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                progressBar.dismiss();

                if (e == null) {
                    imageParseFileList.add(mParseFile);
                    currentUploadIndex += 1;
                    if (currentUploadIndex == imageObjects.size()) {
                        saveResourceObject();
                    } else uploadAllParseFiles();
                } else {
                    Log.e(TAG, "Could not save parse file. Error: " + e.getMessage() + " " + e.getCode());
                    Toast.makeText(MainScreenActivity.this, "Oops, something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }
        }, new ProgressCallback() {
            @Override
            public void done(Integer integer) {
                Log.i(TAG, "Done: " + integer);
                progressBar.setProgress(integer);
            }
        });
    }*/

    private void saveResourceObject() {
        showProgressDialog("Saving resource...");

        ParseObject resourceObject = new ParseObject("Resources");

        if (QuizMateApp.currentDisplayName != null) {
            resourceObject.put("userDisplayName", QuizMateApp.currentDisplayName);
        } else {
            resourceObject.put("userDisplayName", "Anonymous");
        }
        resourceObject.put("userInstituteName", QuizMateApp.currentInstituteName);
        resourceObject.put("userInstituteObjectId", QuizMateApp.currentInstituteObjectId);
        resourceObject.put("resourceCourseId", resourceCourseCode);
        resourceObject.put("resourceTag", resourceTag);
        resourceObject.put("resourceYear", resourceYear);
        resourceObject.put("resourceImages", imageUrlList);
        resourceObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    addCourseCodeToDataBase(QuizMateApp.currentInstituteName, QuizMateApp.currentInstituteObjectId, resourceCourseCode);
                } else {
                    progressDialog.dismiss();
                    Log.e(TAG, "Could not save resource.Error: " + e.getCode() + " " + e.getMessage());
                    Toast.makeText(MainScreenActivity.this, "Oops, something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addCourseCodeToDataBase(final String courseInstituteName, final String courseInstituteObjectId, final String courseCode) {
        ParseQuery<ParseObject> courseListQuery = ParseQuery.getQuery("Courses");
        courseListQuery.whereEqualTo("courseCode", courseCode);
        courseListQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    if (objects.size() > 0) {
                        ParseObject tempCourseObject = objects.get(0);
                        tempCourseObject.increment("entries");
                        tempCourseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                progressDialog.dismiss();
                                Toast.makeText(MainScreenActivity.this, "Resource uploaded.", Toast.LENGTH_SHORT).show();
                                if (e == null) Log.i(TAG, "Course Updated in List");
                                else Log.i(TAG, "Course could not be Updated in List");
                            }
                        });
                    } else {
                        ParseObject courseObject = new ParseObject("Courses");
                        courseObject.put("instituteName", courseInstituteName);
                        courseObject.put("instituteObjectId", courseInstituteObjectId);
                        courseObject.put("courseCode", courseCode);
                        courseObject.put("entries", 0);
                        courseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                progressDialog.dismiss();
                                Toast.makeText(MainScreenActivity.this, "Resource uploaded.", Toast.LENGTH_SHORT).show();
                                if (e == null) Log.i(TAG, "Course Added in List");
                                else Log.i(TAG, "Course Not Saved in List");
                            }
                        });
                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(MainScreenActivity.this, "Resource uploaded.", Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "Can't Query Courses");
                }
            }
        });
    }

    private void setAppRate() {
        RateTheApp.onStart(this);
        RateTheApp.showRateDialogIfNeeded(this);
    }

    private void saveInstallationToParse() {
        ParseInstallation mParseInstallation = ParseInstallation.getCurrentInstallation();
        if (QuizMateApp.currentDisplayName != null)
            mParseInstallation.put("userName", QuizMateApp.currentDisplayName);
        else mParseInstallation.put("userName", "Anonymous");
        if (QuizMateApp.currentInstituteName != null)
            mParseInstallation.put("instituteName", QuizMateApp.currentInstituteName);
        else
            mParseInstallation.put("instituteName", "Anonymous");
        mParseInstallation.saveInBackground();
    }

    private class MainScreenPagerAdapter extends FragmentPagerAdapter {

        ArrayList<Fragment> fragments;

        public MainScreenPagerAdapter(FragmentManager fm) {
            super(fm);

            fragments = new ArrayList<>();
            fragments.add(new SearchFragment());
            fragments.add(new CourseFragment());
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.search_fragment);
                case 1:
                    return getResources().getString(R.string.course_fragment);
                default:
                    return null;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }

        return true;
    }

    private class imageAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;

        private imageAdapter() {
            mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (imageObjects.size() >= 8) {
                return imageObjects.size();
            } else return imageObjects.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return imageObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View mView = mLayoutInflater.inflate(R.layout.item_image_grid, parent, false);
            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.mImage = (SquareImageView) mView.findViewById(R.id.picture);

            if (position == imageObjects.size()) {
                mViewHolder.mImage.setImageResource(R.drawable.ic_attach_64_32);

                mViewHolder.mImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MaterialDialog chooseDialog = new MaterialDialog.Builder(MainScreenActivity.this)
                                .title("Attach image from")
                                .titleColorRes(R.color.colorPrimaryDark)
                                .customView(R.layout.dialog_picture_chooser, true)
                                .show();

                        View view = chooseDialog.getCustomView();

                        TextView fromCamera = (TextView) view.findViewById(R.id.fromCamera);
                        TextView fromGallery = (TextView) view.findViewById(R.id.fromGallery);

                        fromCamera.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.d(TAG, "From Camera");
                                chooseDialog.dismiss();
                                chooseImage(true);
                            }
                        });

                        fromGallery.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.d(TAG, "From Gallery");
                                chooseDialog.dismiss();
                                chooseImage(false);
                            }
                        });
                    }
                });
            } else {
                mViewHolder.mImage.setImageBitmap(imageObjects.get(position).getmBitmap());
            }

            return mView;
        }

        private class ViewHolder {
            private SquareImageView mImage;
        }
    }

    private boolean chooseImage(boolean fromCamera) {
        if (fromCamera) {
            boolean Error = false;
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            File photo = null;
            try {
                // place where to store camera taken picture
                photo = this.createTemporaryFile("picture", ".jpg");
                photo.delete();
            } catch (Exception e) {
                Log.e(TAG, "Can't create file to take picture!");
                Toast.makeText(MainScreenActivity.this, "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                Error = true;
            }

            if (!Error) {
                mImageUri = Uri.fromFile(photo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                startActivityForResult(intent, CHOOSE_IMAGE_FROM_CAMERA);
            }
        } else {
            Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, CHOOSE_IMAGE_FROM_GALLERY);
        }
        return true;
    }

    private File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && (requestCode == CHOOSE_IMAGE_FROM_GALLERY || requestCode == CHOOSE_IMAGE_FROM_CAMERA)) {

            boolean Error = false;
            Uri mImageFileUri;

            if (requestCode == CHOOSE_IMAGE_FROM_CAMERA) {
                getContentResolver().notifyChange(mImageUri, null);
                mImageFileUri = mImageUri;
            } else {
                getContentResolver().notifyChange(data.getData(), null);
                mImageFileUri = data.getData();
            }

            ContentResolver cr = this.getContentResolver();
            Bitmap bitmap = null;
            try {
                if (requestCode == CHOOSE_IMAGE_FROM_CAMERA)
                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
                else if (requestCode == CHOOSE_IMAGE_FROM_GALLERY)
                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, data.getData());
            } catch (Exception e) {
                Toast.makeText(this, "Failed to load image", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Failed to load image");
                Error = true;
            }

            if (!Error) {
                Bitmap thumbBitmap = ThumbnailUtils.extractThumbnail(bitmap, 64, 64);
                /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                byte[] byteArray = stream.toByteArray();*/

                File mImageFile = getCompressedFile(bitmap, 80);
                //File mImageFile = getUnCompressedFile(mImageFileUri);

                if (mImageFile != null) {
                    String mFileSize = getFileSize(mImageFile);
                    Toast.makeText(MainScreenActivity.this, "Image Size: " + mFileSize, Toast.LENGTH_SHORT).show();
                    imageObjects.add(new ImageObjects(thumbBitmap, mImageFile, mFileSize));

                    mAdapterGrid.notifyDataSetChanged();

                    if (imageObjects.size() == 8)
                        Toast.makeText(MainScreenActivity.this, "Max no. of attachments reached.", Toast.LENGTH_SHORT).show();

                    if (resourceCourseCode.length() > 0 && resourceYear.length() > 0 && imageObjects.size() > 0) {
                        if (resourceTagPos == 4 && resourceTag.length() == 0) {
                            positiveAction.setEnabled(false);
                        } else positiveAction.setEnabled(true);

                    } else positiveAction.setEnabled(false);
                } else
                    Toast.makeText(MainScreenActivity.this, "Cannot Load Image", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private File getUnCompressedFile(Uri mImageFileUri) {
        String filePath = DocumentHelper.getPath(MainScreenActivity.this, mImageFileUri);
        if (filePath == null || filePath.isEmpty()) {
            Toast.makeText(MainScreenActivity.this, "Can't access app directory", Toast.LENGTH_SHORT).show();
            return null;
        } else return new File(filePath);
    }

    private File getCompressedFile(Bitmap bitmap, int compression) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date now = new Date();
        String fileNameTemp = formatter.format(now) + ".jpg";

        File tempFile = new File(extStorageDirectory, fileNameTemp);
        try {
            outStream = new FileOutputStream(tempFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, compression, outStream);
            outStream.flush();
            outStream.close();
            return tempFile;
        } catch (Exception e) {
            Log.e(TAG, "Image Compression Error: " + e.getMessage());
            return null;
        }
    }

    private String getFileSize(File mImageFile) {
        long Filesize = getFolderSize(mImageFile) / 1024;//call function and convert bytes into Kb
        if (Filesize >= 1024)
            return Filesize / 1024 + " Mb";
        else
            return Filesize + " Kb";
    }

    private long getFolderSize(File mFile) {
        long size = 0;
        if (mFile.isDirectory()) {
            for (File file : mFile.listFiles()) {
                size += getFolderSize(file);
            }
        } else {
            size = mFile.length();
        }
        return size;
    }

    /*private void goToLogin() {
        Intent goToLoginSignupActivity = new Intent(MainScreenActivity.this, LoginSignupActivity.class);
        startActivityForResult(goToLoginSignupActivity, LOGIN_REQUEST_CODE);
    }*/

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.colorPrimaryDark)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private void showProgressBar(String message) {
        progressBar = new MaterialDialog.Builder(this)
                .title(message)
                .titleColorRes(R.color.colorPrimaryDark)
                .content("Uploading large files, This may take a while.")
                .progress(true, 0)
                .cancelable(false)
                .progressIndeterminateStyle(true)
                .show();

    }

    private void updateProgressBar(String message) {
        progressBar.setTitle(message);

    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(QuizMateApp.showCollegeChooseDialog))
                showCollegeChooserDialog();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }
}
