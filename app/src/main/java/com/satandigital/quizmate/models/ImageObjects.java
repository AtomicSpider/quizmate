package com.satandigital.quizmate.models;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by Sanat Dutta on 10/31/2015.
 * http://www.satandigital.com/
 */
public class ImageObjects {

    public Bitmap mBitmap;
    public File mImageFile;
    public String mFileSize;

    public ImageObjects(Bitmap mBitmap, File mImageFile, String mFileSize) {
        this.mBitmap = mBitmap;
        this.mImageFile = mImageFile;
        this.mFileSize = mFileSize;
    }

    public Bitmap getmBitmap() {
        return mBitmap;
    }

    public void setmBitmap(Bitmap mBitmap) {
        this.mBitmap = mBitmap;
    }

    public File getmImageFile() {
        return mImageFile;
    }

    public void setmImageFile(File mImageFile) {
        this.mImageFile = mImageFile;
    }

    public String getmFileSize(){
        return mFileSize;
    }

    public void setmFileSize(String mFileSize){
        this.mFileSize = mFileSize;
    }
}
